# Encontrei o 'Habilitar VT-x/AMD-V Aninhado' mas não consigo ativa no VirtualBox no linux.

### Procedimento para ativação de virtualização 'Habilitar VT-x/AMD-V Aninhado' no VirtualBox.

### No terminal execute o seguinte comando para lista as maquinas virtuais.

```shell 
vboxmanage list vms
```
### Após a execução como vai apresentar exemplo a baixo.
```shell
wellison@wellison:~$ vboxmanage list vms
"Wellison-Windows10" {655b863a-edb3-4dcf-b247-756e20b84351}
"Template-rocky-8.8" {eb7b2a36-8669-4225-8463-3102330c51b3}
"Template-Windows10" {934446a2-7ea3-47d7-b06b-f8a8173a5231}
"rocky-8.8-zabbix-docker" {9827736e-849b-4b8f-8fdb-19b1ee4afd7b}
```

### Para ativação do 'Habilitar VT-x/AMD-V Aninhado' no VirtualBox na maquina virtual que é "rocky-8.8-zabbix-docker", execute o proximo comando que é:
```shell
vboxmanage modifyvm "rocky-8.8-zabbix-docker" --nested-hw-virt=on
```
