**Procedimentos básicos para configuração de IP estático**\
**Vamos realizar uma configuração de rede para que o sistema use IP estático.**\
**Siga os passos a seguir (meu exemplo usa Ubuntu Server):**

**1 – Entre no diretório netplan pelo terminal:**
```shell
$ cd /etc/netplan
```
**2 – Verifique o nome do arquivo de configuração existente em sua distribuição:**
```shell
$ ls
```
**Por exemplo, no Ubuntu 20.04 o arquivo de configuração padrão é o 01-network-manager-all.yaml**

**Caso nenhum arquivo YAML tenha sido configurado durante a instalação da distribuição,** \
**e nada apareça neste diretório, você pode gerar um novo arquivo de configurações**\
**para os renderizadores emitindo o comando a seguir:**
```shell
$ sudo netplan generate
```
**Ou simplesmente criar um novo arquivo usando um editor de textos, que use a extensão .yaml.**\
**Recomenda-se inicias o nome do arquivo com um número também.**

**3 – Fazer backup de arquivo de configuração atual:**
```shell
sudo cp 01-network-manager-all.yaml 01-network-manager-all.yaml.bkp
```
**4 – Abrir o arquivo de configuração para edição (use o editor de textos de sua preferência):**
```shell
sudo vim /etc/netplan/01-network-manager-all.yaml
```
**5 – Configure o arquivo como segue:**
```shell
network:
   version: 2
   renderer: networkd
   ethernets:
      enp0s3:
         dhcp4: no
         dhcp6: no
         addresses: [192.168.100.2/24]
         gateway4: 192.168.100.1
         nameservers:
            addresses: [192.168.100.1,8.8.8.8]
```
**Neste exemplo temos:**

**renderer:    Renderizador (daemon de rede). Aceita os valores NetworkManager e networkd.**

**O padrão é networkd. Pode ser aplicada globalmente ou para um dispositivo específico.**\
**ethernets: Seção das interfaces de rede a configurar**\
**enp0s3:      Nome de uma interface de rede a configurar**\
**dhcp4:       Propriedades da interface para o DHCP versão 4**\
**dhcp6:       Propriedades da interface para o DHCP versão 6**\
**addresses:   Lista de IPs a serem atribuídos**\
**gateway4:    Endereço do gateway padrão da rede**\
**nameservers: Seção de Servidores DNS e domínios de busca a usar**

**6 – Salve e saia do arquivo.**

**7 – Testando a sintaxe do arquivo:**

```shell
$ sudo netplan try
```

**8 – Aplicar a configuração:**

```shell
$ sudo netplan apply
```

**9 – Finalmente, verificar se o IP foi alterado de acordo com nossa necessidade:**

```shell
$ ip addr show
```

**Configuração realizada com sucesso. Mas e se nosso servidor possuir**
**duas interfaces de rede, por exemplo as interfaces enp0s3 e enp0s8? Neste**
**caso, basta adicionar as interfaces no mesmo arquivo:**

```shell
network:
   version: 2
   renderer: networkd
   ethernets:
      enp0s3:
         dhcp4: no
         addresses: [192.168.100.2/24]
         gateway4: 192.168.100.1
         nameservers:
            addresses: [192.168.100.1,8.8.8.8]
      enp0s8:
         dhcp4: no
         addresses: [192.168.100.4/24]
         gateway4: 192.168.100.1
         nameservers:
            addresses: [192.168.100.1,8.8.8.8]
```

**É isso! Outras configurações de rede que podem ser realizadas com o netplan incluem:**

**macaddress: Endereço físico da interface, no formato “XX:XX:XX:XX:XX:XX”**\
**mtu: Valor de MTU da interface**\
**gateway6: gateway padrão da rede, IPv6**\
**Também é possível configurar rotas e redes sem fio, criar bonds e configurar vlans.**\
**Veremos essas opções em uma próxima lição.**

**Maiores informações sobre o netplan podem ser obtidas em sua página de manual:**

```shell
$ man netplan
```
